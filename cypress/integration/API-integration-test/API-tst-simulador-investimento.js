/// <reference types="Cypress" />

const searchterm = "coffee";// The first letter should be uppercase!!
const searchterminvalid = "iohji";
const contactUs = "https://n1866.secure.force.com/contactus/CU_Home?brand=coffeemate&consumerContactOrigin=www.coffeemate.com&country=US&countryCode=41&language=en-US&market=US&selectedLanguage=English"
const contactUsFooter = "https://n1866.secure.force.com/contactus/CU_Home_Local?brand=tombstonepizza&consumerContactOrigin=www.coffeemate.com&country=US&market=US&selectedLanguage=English"
const chat = 'liveagent_button_online_5730N000000k9gO';
const facebook = 'https://www.facebook.com/CoffeemateUSA';
const instagram = 'https://www.instagram.com/coffeemate/';
// const pinterest = 'https://www.pinterest.com/stouffers';
const twitter = 'https://twitter.com/Coffeemate';
const youtube = 'https://www.youtube.com/user/CoffeemateUSA';

context('Automated API Test: "Simulador de Investimento Sicredi', () => {
    
    beforeEach(() => {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        });
        //cy.visit('https://www.coffeemate.com');
    })
    
        it('Validate API Response', () => {
          const simulador = cy.request('GET', 'http://5b847b30db24a100142dce1b.mockapi.io/api/v1/simulador').as('simulador')
          
          cy.get('@simulador').should(response => {
            expect(response.body).to.be.an('object');
            expect(response.body.id).to.be.eq(1);
            expect(response.body.meses).to.be.an('array');
            expect(response.body.meses).to.have.length.above(0);
            expect(response.body.meses[0]).to.be.eq('112');
            expect(response.body.meses[1]).to.be.eq('124');
            expect(response.body.meses[2]).to.be.eq('136');
            expect(response.body.meses[3]).to.be.eq('148'); 
            expect(response.body.valor).to.have.length.above(0);
            expect(response.body.valor[0]).to.be.eq('2.802');
            expect(response.body.valor[1]).to.be.eq('3.174');
            expect(response.body.valor[2]).to.be.eq('3.564');
            expect(response.body.valor[3]).to.be.eq('3.971');

          });

          simulador
            .its('status')
            .should('be.eql', 200);
        })

})